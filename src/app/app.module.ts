import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Configuration } from './app.config';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatToolbarModule, MatTableModule, MatButtonModule, MatDatepickerModule,
  MatDialogModule, MatFormFieldModule, MatAutocompleteModule,
  MatInputModule, MatSnackBarModule, MatListModule, MatCardModule,
  MatIconModule, MatPaginatorModule, MatSlideToggleModule, MatNativeDateModule
} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';
import { NewComponent } from './book/new/new.component';

const appRoutes: Routes = [

  {
    path: 'book',
    component: BookComponent,
    children: []
  },
  {
    path: '',
    redirectTo: 'book',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    NewComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    HttpModule,
    BrowserModule,
    MatToolbarModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxDatatableModule
  ],
  providers: [FormBuilder, HttpModule, Configuration],
  bootstrap: [AppComponent,
    BookComponent,
    NewComponent]
})
export class AppModule { }
