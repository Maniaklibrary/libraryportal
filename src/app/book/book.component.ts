import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSort, PageEvent } from '@angular/material';
import { Configuration } from '../app.config';
import * as _ from 'underscore';

import { BookService } from './book.service';
import { NewComponent } from './new/new.component';

@Component({
  selector: 'app-root',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
  providers: [BookService]
})
export class BookComponent implements OnInit {

  books = [];

  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 100];
  total = 0;

  curPage = 1
  selectedCount = 1
  rowCount = 20

  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  constructor(public dialog: MatDialog,
    private config: Configuration,
    public _bookService: BookService) {
  }

  ngOnInit() {
    var options = { pageIndex: this.pageIndex, pageSize: this.pageSize };
    this.getAll(options);
  }

  getAll(options) {
    this._bookService.getAll(options).subscribe(result => {
      if (result.status) {
        this.books = result.books
        this.total = result.total
      }
    });
  }

  addBook(): void {
    let dialogRef = this.dialog.open(NewComponent, {
      width: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
      var options = { pageIndex: this.pageIndex, pageSize: this.pageSize };
      this.getAll(options);
    });
  }

  delete(book) {
    if (!book) {
      return
    }

    this._bookService.delete(book).subscribe(result => {
      var options = { pageIndex: this.pageIndex, pageSize: this.pageSize };
      this.getAll(options);
    });
  }

  edit(book) {
    console.log(book)

    let dialogRef = this.dialog.open(NewComponent, {
      width: '450px',
      data: book
    });

    dialogRef.afterClosed().subscribe(result => {
      var options = { pageIndex: this.pageIndex, pageSize: this.pageSize };
      this.getAll(options);
    });
  }

  pageEvent(event) {
    var options = event;
    this.pageSize = options.pageSize
    this.pageIndex = options.pageIndex
    this.getAll(options);
  }

}