import { Injectable, OpaqueToken } from "@angular/core"
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";
import { Configuration } from '../app.config';

@Injectable()
export class BookService {

    apiEndpoint = "";

    constructor(private http: Http, private config: Configuration) {
        this.apiEndpoint = config.API_MAIN;
    }

    getAll(options): Observable<any> {
        return this.http.post(this.apiEndpoint + 'Book/getAll', options)
            .map((response: Response) => {
                return response.json()
            });
    }

    delete(book): Observable<any> {
        return this.http.post(this.apiEndpoint + 'Book/delete', book)
            .map((response: Response) => {
                return response.json()
            });
    }
}