import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import * as Moment from 'moment';

import { NewService } from './new.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css'],
  providers: [NewService]
})
export class NewComponent implements OnInit {

  form: FormControl = new FormControl();

  Categories;
  name;
  author;
  categorySelected = '';
  book;
  publishedDate;
  user;
  edit = false

  filteredCategories: Observable<string[]>;

  constructor(private _newService: NewService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<NewComponent>,
    public snackBar: MatSnackBar) {

    if (data) {
      this.book = data;
      var formatedDate = new Date(this.book.publishDate);
      console.log(formatedDate)

      this.name = this.book.name
      this.author = this.book.author
      this.user = this.book.user
      this.publishedDate = formatedDate.toISOString()
      this.categorySelected = this.book.category
      this.edit = true;
    }
  }

  ngOnInit() {
    this._newService.getAll().subscribe(result => {
      if (result.status) {
        this.Categories = result.categories;

        this.filteredCategories = this.form.valueChanges
          .startWith(null)
          .map(val => val ? this.filter(val) : this.Categories.slice());
      }
    });
  }

  filter(name: string): string[] {
    return this.Categories.filter(category =>
      category.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }


  save() {
    if (!this.name) {
      this.snackBar.open("Name is required!", null, { duration: 2000 });
      return;
    }
    if (!this.author) {
      this.snackBar.open("Author is required!", null, { duration: 2000 });
      return;
    }
    if (!this.categorySelected) {
      this.snackBar.open("Category is required!", null, { duration: 2000 });
      return;
    }
    if (!this.publishedDate) {
      this.snackBar.open("Published Date is required!", null, { duration: 2000 });
      return;
    }

    var data = {
      name: this.name,
      author: this.author,
      category: this.categorySelected,
      publishDate: Moment(this.publishedDate).format('YYYY-MM-DD'),
      user: this.user
    }

    console.log("SAVE", data);

    this._newService.insert(data).subscribe(result => {
      if (result.status) {
        this.dialogRef.close();
      }
      else {
        this.snackBar.open(result.error, null, { duration: 2000 });
      }
    });

  }

  update() {
    if (!this.name) {
      this.snackBar.open("Name is required!", null, { duration: 2000 });
      return;
    }
    if (!this.author) {
      this.snackBar.open("Author is required!", null, { duration: 2000 });
      return;
    }
    if (!this.categorySelected) {
      this.snackBar.open("Category is required!", null, { duration: 2000 });
      return;
    }

    var editBook = this.book;

    editBook.name = this.name
    editBook.author = this.author
    editBook.category = this.categorySelected
    editBook.publishDate = Moment(this.publishedDate).format('YYYY-MM-DD')
    editBook.user = this.user

    console.log("UPDATE", editBook);

    this._newService.update(editBook).subscribe(result => {
      if (result.status) {
        this.dialogRef.close();
      }
      else {
        this.snackBar.open(result.error, null, { duration: 2000 });
      }
    });

  }

}
