import { Injectable, OpaqueToken } from "@angular/core"
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";
import { Configuration } from '../../app.config';

@Injectable()
export class NewService {

    apiEndpoint = "";

    constructor(private http: Http, private config: Configuration) {
        this.apiEndpoint = config.API_MAIN;
    }

    getAll(): Observable<any> {
        return this.http.get(this.apiEndpoint + 'Categories/getAll')
            .map((response: Response) => {
                return response.json()
            });
    }

    insert(book: any): Observable<any> {
        return this.http.post(this.apiEndpoint + 'Book/new', book)
            .map((response: Response) => {
                return response.json()
            });
    }

    update(book: any): Observable<any> {
        return this.http.post(this.apiEndpoint + 'Book/update', book)
            .map((response: Response) => {
                return response.json()
            });
    }
}